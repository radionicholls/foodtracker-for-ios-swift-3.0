//
//  RatingControl.swift
//  Food Tracker
//
//  Created by Peter Nicholls on 11/07/2016.
//  Copyright © 2016 Peter Nicholls. All rights reserved.
//

import UIKit

// removes magic numbers
struct ButtonConstants {
    var x: Int
    var y: Int
    var width: Int
    var height: Int
    var padding: Int
    var count: Int
    var containerWidth: Int {
        get {
            var computedWidth = 0
            for _ in 0..<count {
                computedWidth = computedWidth + width + padding
            }
            return computedWidth - padding
        }
    }
    var frame:CGRect {
        get { return CGRect(x: x, y: y, width: width, height: height) }
    }
}

class RatingControl: UIView {

    // MARK: Properties
    var rating = 0 {
        didSet {
            setNeedsLayout()
        }
    }

    var ratingButtons = [UIButton]()

    // Constants for button factory etc.
    let btn = ButtonConstants(x: 0, y: 0, width: 44, height: 44, padding: 5, count: 5)

    // MARK: Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        let filledStar = UIImage(named: "filledStar")
        let emptyStar = UIImage(named: "emptyStar")

        // Button Factory
        for _ in 0..<btn.count {
            let rateButton = UIButton(frame: btn.frame)

            rateButton.setImage(emptyStar, for: .normal)
            rateButton.setImage(filledStar, for: .selected)
            rateButton.setImage(filledStar, for: .highlighted)

            rateButton.adjustsImageWhenHighlighted = false

            rateButton.addTarget(self, action: .ratingButtonTapped, for: .touchDown)
            ratingButtons += [rateButton]
            addSubview(rateButton)
        }

    }

    override func layoutSubviews() {
        var buttonFrame = btn.frame

        for (index, button) in ratingButtons.enumerated() {
            buttonFrame.origin.x = CGFloat(index * (btn.width + btn.padding))
            button.frame = buttonFrame
        }

        updateButtonSelectionStates()
    }

    override func intrinsicContentSize() -> CGSize {
        return CGSize(width: btn.containerWidth,
                      height: btn.height)
    }

    func ratingButtonTapped(button: UIButton) {
        rating = ratingButtons.index(of: button)! + 1 // +1 because 0 index.

        updateButtonSelectionStates()
    }

    func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated() {
            // If the index of a button is less than the rating, that button should be selected.
            button.isSelected = index < rating
        }
    }
}

// Helpers

// replaces clunky obj-c call in addTarget
private extension Selector {
    static let ratingButtonTapped =
        #selector(RatingControl.ratingButtonTapped(button:))
}



